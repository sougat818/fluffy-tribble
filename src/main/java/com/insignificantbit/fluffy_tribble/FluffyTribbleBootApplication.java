package com.insignificantbit.fluffy_tribble;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FluffyTribbleBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(FluffyTribbleBootApplication.class, args);
	}

}
